SQL scripts for grounding
-------------------------

* create.sql -- Create schema, facts and rules tables.  
* load.sql -- Load csv data files.  
* ground.sql -- Create grounding procedures.  
* drop.sql -- Drop schema.  
* qc.sql -- Create quality control procedures.  
* debug.sql -- Debugging utilities.  
